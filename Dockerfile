FROM index.docker.io/library/rust:latest AS build
ARG APPNAME=app
RUN apt-get update && apt-get -y install musl-tools upx
RUN rustup target add x86_64-unknown-linux-musl

WORKDIR /buildenv
RUN USER=root cargo new -q --vcs none --bin $APPNAME
WORKDIR /buildenv/$APPNAME
COPY Cargo.toml .
COPY Cargo.lock .
RUN cargo build --release --target x86_64-unknown-linux-musl

RUN rm src/*.rs && rm -f ./target/release/deps/$APPNAME*
COPY src/ src/
RUN cargo build --release --target x86_64-unknown-linux-musl && strip ./target/x86_64-unknown-linux-musl/release/$APPNAME && upx --best -q --lzma ./target/x86_64-unknown-linux-musl/release/$APPNAME
RUN mkdir /build && mv ./target/x86_64-unknown-linux-musl/release/$APPNAME /build/app

FROM scratch
COPY --from=build /build/app app
ENTRYPOINT ["./app"]
