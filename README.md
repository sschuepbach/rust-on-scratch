# Rust on Scratch

Template for a small-as-possible Rust application's Docker image

Set `APPNAME` build arg to use the correct bin name of your application, e.g.:

```sh
docker build -t my-app --build-arg APPNAME=my-app .
```

=> [Hints for reducing binary size](https://github.com/johnthagen/min-sized-rust)
